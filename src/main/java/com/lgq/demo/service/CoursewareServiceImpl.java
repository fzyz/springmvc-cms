package com.lgq.demo.service;

import com.lgq.demo.common.PageLimit;
import com.lgq.demo.dao.CoursewareDAO;
import com.lgq.demo.model.Courseware;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by liguoqing on 2016/1/11.
 */
@Service("coursewareService")
public class CoursewareServiceImpl implements CoursewareService {

    @Autowired
    private CoursewareDAO coursewareDAO;

    @Override
    public PageLimit<Courseware> get(Map<String, String> conditionMap, PageLimit<Courseware> pageLimit) {
        return coursewareDAO.get(conditionMap, pageLimit);
    }

    @Override
    public Courseware get(String id) {
        return coursewareDAO.get(id);
    }

    @Override
    public List<Courseware> get(List<String> ids) {
        return coursewareDAO.get(ids);
    }

    @Override
    public String update(Courseware courseware) {
        return coursewareDAO.update(courseware);
    }

    @Override
    public String save(Courseware courseware) {
        return coursewareDAO.save(courseware);
    }

    @Override
    public String delete(String id) {
        return coursewareDAO.delete(id);
    }

    @Override
    public String delete(List<String> ids) {
        return coursewareDAO.delete(ids);
    }

    @Override
    public boolean checkDuplicate(String code) {
        List<Courseware> coursewares = coursewareDAO.getByCode(code);
        if(CollectionUtils.isNotEmpty(coursewares)) {
            return true;
        }
        return false;
    }

}
